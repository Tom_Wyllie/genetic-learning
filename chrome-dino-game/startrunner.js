// Basic script to create a new Runner object once the page has loaded in
document.addEventListener("DOMContentLoaded", function(event) {
  simulation = new Runner('#simulation-container');

  var herdSize = 10;
  var agents = [];

  for (var i = 0; i < herdSize; i++) {
    agents.push(cheatAgent);
  }

  simulation.setAgents(agents);
  simulation.startSimulation();
});

function cheatAgent(nextObstacleDistance) {
  var response = {'duck': false, 'jump': false};

  if (nextObstacleDistance < 16) {
    response['jump'] = true;
  }

//  if (nextObstacleDistance > 32) {
//    response['duck'] = true;
//  }

  return response
}